# FrontendForecast

This app contains the following features:

 � displaying 5 days forecast using your geolocation (temperature can be displayed as fahrenheit or celsius)
 
 � displaying 5 days forecast for city  (temperature can be displayed as fahrenheit or celsius)
 
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.4.

## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

##New features

We can simple add forecast weather for a specific day, which will contain weather changes chart in relation of the hour
import {TestBed, async} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ForecastComponent} from './forecast.component';
import {ForecastDayChartComponent} from './forecast-day-chart/forecast-day-chart.component';
import {ForecastDaysComponent} from './forecast-days/forecast-days.component';
import {ForecastDayDetailsComponent} from './forecast-day-details/forecast-day-details.component';
import {ForecastApiService} from './services/forecast-api.service';
import {LocationService} from '../common/services/location.service';
import {FormsModule} from '@angular/forms';
import {ChartsModule} from 'ng2-charts';
import {NgxSpinnerModule} from 'ngx-spinner';
import {ForecastApiMocService} from './services/forecast-api.moc.service';
import {LocationMocService} from '../common/services/location.moc.service';

describe('ForecastComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                ForecastComponent,
                ForecastDaysComponent,
                ForecastDayChartComponent,
                ForecastDayDetailsComponent
            ],
            imports: [
                RouterTestingModule,
                HttpClientTestingModule,
                FormsModule,
                ChartsModule,
                NgxSpinnerModule,
            ],
            providers: [
                {provide: ForecastApiService, useClass: ForecastApiMocService},
                {provide: LocationService, useClass: LocationMocService}
            ]
        }).compileComponents();
    }));

    it('should create the forecast', () => {
        const fixture = TestBed.createComponent(ForecastComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});

export interface Forecast {
  city: City;
  list: DayForecast[];
}

export interface DayForecast {
  clouds: number;
  deg: number;
  dt: number;
  humidity: number;
  pressure: number;
  speed: number;
  temp: Temperature;
  weather: Weather[];
}

interface Temperature {
  day: number;
  night: number;
  max: number;
  min: number;
  morn: number;
  eve: number;
}

interface Weather {
  description: string;
  icon: string;
}

interface City {
  name: string;
}

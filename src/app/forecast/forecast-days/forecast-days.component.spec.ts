import {TestBed, async} from '@angular/core/testing';
import {ForecastDaysComponent} from './forecast-days.component';

describe('ForecastDayChartComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                ForecastDaysComponent
            ]
        }).compileComponents();
    }));

    it('should create the forecast-days', () => {
        const fixture = TestBed.createComponent(ForecastDaysComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});

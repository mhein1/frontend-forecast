import {Component, EventEmitter, Input, Output} from '@angular/core';
import {DayForecast, Forecast} from '../interfaces/forecast.interface';

@Component({
  selector: 'app-forecast-days',
  templateUrl: './forecast-days.component.html',
  styleUrls: ['./forecast-days.component.scss']
})
export class ForecastDaysComponent {

  @Input() forecast: Forecast = null;

  @Input() isCelsius: boolean;

  @Output() isCelsiusChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output() dayForecastChanged: EventEmitter<DayForecast> = new EventEmitter();

  dayForecastSelect = [true, false, false, false, false];

  toggleForecastDay(forecastDay: DayForecast, currentSelected: number) {
    const lastSelected = this.dayForecastSelect.findIndex((el) => el === true);
    this.dayForecastSelect[lastSelected] = false;
    this.dayForecastSelect[currentSelected] = true;
    this.dayForecastChanged.emit(forecastDay);
  }
}

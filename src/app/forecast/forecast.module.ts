import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ForecastComponent} from './forecast.component';
import {ForecastDaysComponent} from './forecast-days/forecast-days.component';
import {ForecastDayDetailsComponent} from './forecast-day-details/forecast-day-details.component';
import { ForecastDayChartComponent} from './forecast-day-chart/forecast-day-chart.component';
import {ForecastApiService} from './services/forecast-api.service';
import {LocationService} from '../common/services/location.service';
import {ChartsModule} from 'ng2-charts';
import {FormsModule} from '@angular/forms';
import {NgxSpinnerModule} from 'ngx-spinner';

const routes: Routes = [
  {
    path: '',
    component: ForecastComponent,
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), ChartsModule, FormsModule, NgxSpinnerModule],
  declarations: [ForecastComponent, ForecastDaysComponent, ForecastDayDetailsComponent, ForecastDayChartComponent],
  providers: [ForecastApiService, LocationService],
})
export class ForecastModule {
}

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {DayForecast, Forecast} from '../interfaces/forecast.interface';

@Component({
  selector: 'app-forecast-day-details',
  templateUrl: './forecast-day-details.component.html',
  styleUrls: ['./forecast-day-details.component.scss']
})
export class ForecastDayDetailsComponent {

  @Input() dayForecast: DayForecast;

  @Input() forecast: Forecast;

  @Input() isLocked: boolean;

  @Input() isCelsius: boolean;

  @Output() isCelsiusChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output() submit: EventEmitter<string> = new EventEmitter<string>();

}

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Geolocation} from '../interfaces/geolocation.interface';
import {Forecast} from '../interfaces/forecast.interface';

@Injectable()
export class ForecastApiService {

    forecastKey = '767a7cce68ed2b3098d41e24364ec56c';
    cnt = 5;
    baseUrl = 'http://api.openweathermap.org/data/2.5/forecast/daily';

    constructor(private http: HttpClient) {
    }

    getForecastByCityNameCelsius(name: string): Observable<Forecast> {
        return this.http.get<Forecast>
        (`${this.baseUrl}?q=${name}&cnt=${this.cnt}&units=metric&appid=${this.forecastKey}`);
    }

    getForecastByGeographicCelsius(geolocation: Geolocation): Observable<Forecast> {
        return this.http.get<Forecast>
        (`${this.baseUrl}?lat=${geolocation.lat}&lon=${geolocation.lon}&cnt=${this.cnt}&units=metric&appid=${this.forecastKey}`);
    }

    getForecastByCityNameFahrenheit(name: string): Observable<Forecast> {
        return this.http.get<Forecast>(`${this.baseUrl}?q=${name}&cnt=${this.cnt}&appid=${this.forecastKey}`);
    }

    getForecastByGeographicFahrenheit(geolocation: Geolocation): Observable<Forecast> {
        return this.http.get<Forecast>
        (`${this.baseUrl}?lat=${geolocation.lat}&lon=${geolocation.lon}&cnt=${this.cnt}&appid=${this.forecastKey}`);
    }
}

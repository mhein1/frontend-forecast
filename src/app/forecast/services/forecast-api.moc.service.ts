import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Geolocation} from '../interfaces/geolocation.interface';
import {Forecast} from '../interfaces/forecast.interface';

@Injectable()
export class ForecastApiMocService {


    getForecastByCityNameCelsius(name: string): Observable<Forecast> {
        return of(null);
    }

    getForecastByGeographicCelsius(geolocation: Geolocation): Observable<Forecast> {
        return of(null);
    }

    getForecastByCityNameFahrenheit(name: string) {
        return of(null);
    }

    getForecastByGeographicFahrenheit(geolocation: Geolocation): Observable<Forecast> {
        return of(null);
    }
}

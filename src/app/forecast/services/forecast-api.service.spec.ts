import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {ForecastApiService} from './forecast-api.service';

describe('ForecastApiService', () => {
  let forecastService, httpMock;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ForecastApiService]
    });
    forecastService = TestBed.get(ForecastApiService);
    httpMock = TestBed.get(HttpTestingController);
  });

  describe('getForecast', () => {
    it('should return forecast', () => {
      forecastService.getForecastByCityNameCelsius('Gdańsk').subscribe(res => {
        expect(res.list.length).toBe(4);
      });
    });
  });
});

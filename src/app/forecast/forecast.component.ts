import {Component, OnInit} from '@angular/core';
import {ForecastApiService} from './services/forecast-api.service';
import {Forecast, DayForecast} from './interfaces/forecast.interface';
import {catchError, finalize, switchMap, tap} from 'rxjs/internal/operators';
import {Geolocation} from './interfaces/geolocation.interface';
import {LocationService} from '../common/services/location.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
    selector: 'app-forecast',
    templateUrl: './forecast.component.html',
})
export class ForecastComponent implements OnInit {

    forecast: Forecast = null;
    dayForecast: DayForecast = null;
    isLocked = false;
    cityName = null;
    geolocation: Geolocation = {
        lat: null,
        lon: null
    };
    isGeolocation = true;
    private _isCelsius = true;

    get isCelsius() {
        return this._isCelsius;
    }

    set isCelsius(val: boolean) {
        if (val) {
            this.getForecastCelsius();
        } else {
            this.getForecastFahrenheit();
        }
        this._isCelsius = val;
    }

    constructor(private forecastApiService: ForecastApiService,
                private locationService: LocationService,
                private spinnerService: NgxSpinnerService) {
    }

    ngOnInit() {
        this.spinnerService.show();
        this.locationService.getLocation().pipe(switchMap(rep => {
            this.geolocation.lon = rep.longitude;
            this.geolocation.lat = rep.latitude;
            return this.forecastApiService.getForecastByGeographicCelsius(this.geolocation);
        })).pipe(catchError(() => {
            this.cityName = 'Gdańsk';
            this.isGeolocation = false;
            return this.forecastApiService.getForecastByCityNameCelsius(this.cityName);
        })).subscribe(res => {
            this.spinnerService.hide();
            this.forecast = res;
            this.dayForecast = res.list[0];
        });
    }


    public onSubmit(cityName: string) {
        this.isLocked = true;
        this.forecastApiService.getForecastByCityNameCelsius(cityName)
            .pipe(finalize(() => this.isLocked = false))
            .subscribe((res: Forecast) => {
                this.isGeolocation = false;
                this.cityName = cityName;
                this.forecast = res;
                this.dayForecast = res.list[0];
            });
    }

    private getForecastCelsius() {
        if (this.isGeolocation) {
            this.forecastApiService.getForecastByGeographicCelsius(this.geolocation).subscribe((res: Forecast) => {
                this.forecast = res;
                this.dayForecast = res.list[0];
                this.isGeolocation = true;
            });
        } else {
            this.forecastApiService.getForecastByCityNameCelsius(this.cityName).subscribe((res: Forecast) => {
                this.forecast = res;
                this.dayForecast = res.list[0];
            });
        }
    }

    private getForecastFahrenheit() {
        if (this.isGeolocation) {
            this.forecastApiService.getForecastByGeographicFahrenheit(this.geolocation).subscribe((res: Forecast) => {
                this.forecast = res;
                this.dayForecast = res.list[0];
            });
        } else {
            this.forecastApiService.getForecastByCityNameFahrenheit(this.cityName).subscribe((res: Forecast) => {
                this.forecast = res;
                this.dayForecast = res.list[0];
            });
        }
    }
}

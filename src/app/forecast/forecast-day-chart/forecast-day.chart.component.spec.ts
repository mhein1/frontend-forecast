import {TestBed, async} from '@angular/core/testing';
import {ChartsModule} from 'ng2-charts';
import {ForecastDayChartComponent} from './forecast-day-chart.component';

describe('ForecastDayChartComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                ForecastDayChartComponent
            ],
            imports: [
                ChartsModule,
            ]
        }).compileComponents();
    }));

    it('should create the forecast-day-chart', () => {
        const fixture = TestBed.createComponent(ForecastDayChartComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});

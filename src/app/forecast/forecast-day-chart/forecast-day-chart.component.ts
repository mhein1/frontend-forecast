import {Component, Input} from '@angular/core';
import {DayForecast} from '../interfaces/forecast.interface';

@Component({
  selector: 'app-forecast-day-chart',
  templateUrl: './forecast-day-chart.component.html',
  styleUrls: ['./forecast-day-chart.component.scss']
})
export class ForecastDayChartComponent {

  @Input() dayForecast: DayForecast;

  @Input() isCelsius: boolean;

  lineChartColors = [
    {
      backgroundColor: [
        'rgba(255, 255, 255, 0.4)',
        'rgba(255, 255, 255, 0.4)',
        'rgba(255, 255, 255, 0.4)',
        'rgba(255, 255, 255, 0.4)',
      ],
      borderColor: [
        'rgba(255, 255, 255, 0.4)',
        'rgba(255, 255, 255, 0.4)',
        'rgba(255, 255, 255, 0.4)',
        'rgba(255, 255, 255, 0.4)',
      ],
    }
  ];

  chartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
    legend: {
      display: false
    },
    scales: {
      yAxes: [{
        gridLines: {
          display: false,
        },
        ticks: {
          callback: value => (this.isCelsius) ? `${value} °C` : `${value} F`
        }
      }],
      xAxes: [{
        gridLines: {
          display: false,
        },
      }]
    }
  };
}
